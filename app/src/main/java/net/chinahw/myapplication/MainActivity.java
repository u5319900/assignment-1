package net.chinahw.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
/**
 * Created by Dong Luo u5319900 on March 2016.
 */
public class MainActivity extends AppCompatActivity {

    static final String filename = "NotesSaved";
    int noteToDelete;
    File notesfile;
    ArrayList<Note> notesArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notesfile = new File(getFilesDir(),filename);
        if(notesfile.exists()) {
            showTitles();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    //Show titles of notes as items in the ListView
    public void showTitles(){

        //read the ArrayList of notes from the file
        notesArray = readArray();

        //convert to a ArrayList of titles
        ArrayList<String> titles = new ArrayList<>();
        for (int i = 0; i < notesArray.size(); i++) {
            titles.add(notesArray.get(i).title);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, titles);
        final ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setClickable(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent launchNote = new Intent(listView.getContext(), NoteActivity.class);
                launchNote.putExtra("NEW", false);
                launchNote.putExtra("POS", position);
                startActivity(launchNote);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView parent, View v, int position, long id){
                createDialogDelete(position);
                return true;
            }
        });
    }

    public void onClickNewNote(MenuItem item){
        Intent NewNote = new Intent(this, NoteActivity.class);
        NewNote.putExtra("NEW",true);
        startActivity(NewNote);
    }

    public void createDialogDelete(int position) {
        noteToDelete = position;
        DialogDelete d = new DialogDelete();
        d.show(getFragmentManager(), "MyDialogFragment");
    }

    public void delete(){
        notesArray.remove(noteToDelete);
        writeArray(notesArray);
        //update the ListView
        showTitles();
        Toast.makeText(this, "Note Deleted", Toast.LENGTH_SHORT).show();
    }
    /*Pop up a dialog when delete button clicked
    Use the template from Google Android Developer Guide:
    http://developer.android.com/guide/topics/ui/dialogs.html
    */
    public class DialogDelete extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.delete_dialog)
                    .setPositiveButton(R.string.delete_note, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            delete();
                        }
                    })
                 .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                     public void onClick(DialogInterface dialog, int id) {
                         // User cancelled the dialog
                     }
                 });
         // Create the AlertDialog object and return it
            return builder.create();
        }
    }

    //The method to read the ArrayList from the file
    public ArrayList<Note> readArray(){
        ArrayList<Note> array = new ArrayList<>();
        //Read the arraylist
        try{
            FileInputStream fileInputStream = new FileInputStream(notesfile);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            array = (ArrayList) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        }catch(IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException c){
            c.printStackTrace();
        }
        return array;
    }

    //The method to write the ArrayList from the file
    public  void writeArray(ArrayList<Note> array){
        try{
            FileOutputStream fileOutputStream = new FileOutputStream(notesfile);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(array);
            objectOutputStream.close();
            fileOutputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}


