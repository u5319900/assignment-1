package net.chinahw.myapplication;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Dong Luo u5319900 on March 2016.
 */

public class NoteActivity extends AppCompatActivity {

    EditText noteTextField;
    EditText titleTextField;
    boolean newnote;
    boolean  notesaved;
    static final String filename = "NotesSaved";
    FileOutputStream fileOutputStream;
    File notesfile;
    int index;
    ArrayList<Note> notesArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        //assign EditText
        noteTextField = (EditText) findViewById(R.id.editTextNote);
        titleTextField = (EditText) findViewById(R.id.editTexttitle);

        //reference to the file
        notesfile = new File(getFilesDir(),filename);

        //Show notes if not new
        if(getIntent().getExtras().getBoolean("NEW")){
            newnote = true;
        }else{
            notesArray = readArray();
            index = getIntent().getExtras().getInt("POS");
            titleTextField.setText(notesArray.get(index).title);
            noteTextField.setText(notesArray.get(index).content);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_note, menu);
        return true;
    }

    //save the changes of a note or a new note
    public void saveNotes(MenuItem item){

        //Get the current title and note content
        String modifiedText = noteTextField.getText().toString();
        String title = titleTextField.getText().toString();
        if(title.isEmpty()){
            Calendar current = Calendar.getInstance();
            title = "Note created at " + current.get(Calendar.HOUR_OF_DAY) + ":" + current.get(Calendar.MINUTE) + " "
                    + current.get(Calendar.DAY_OF_MONTH) + "/" + (current.get(Calendar.MONTH) + 1) + "/" + current.get(Calendar.YEAR);
        }

        //read ArrayList object from the file
        notesArray = readArray();

        //modify the note ArrayList
       if (newnote && !notesaved){
           index        = notesArray.size();

            //Add the new note to the ArrayList
            Note note    = new Note();
            note.content = modifiedText;
            note.title   = title;
            notesArray.add(note);

        }else{

            //update the note element in the ArrayList
            notesArray.get(index).content = modifiedText;
            notesArray.get(index).title   = title;
        }

        writeArray(notesArray);
        notesaved = true;
        Toast.makeText(this, "Note Saved", Toast.LENGTH_SHORT).show();
    }

    //called when the user clicked "delete"
    public void onClickDeleteNotes(MenuItem item){
        DialogDelete d = new DialogDelete();
        d.show(getFragmentManager(), "MyDialogFragment");
    }

    //delete the current note
    public void deleteNotes(){
        if(newnote && !notesaved){
            //Do nothing
        }else{
            notesArray.remove(index);
            writeArray(notesArray);
        }

        Toast.makeText(this, "Note Deleted", Toast.LENGTH_SHORT).show();
        Intent back = new Intent(this, MainActivity.class);
        startActivity(back);
    }

    //The method to read the ArrayList from the file
    public ArrayList<Note> readArray(){
        ArrayList<Note> array = new ArrayList<>();
        //Read the arraylist
        try{
            FileInputStream fileInputStream = new FileInputStream(notesfile);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            array = (ArrayList) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        }catch(IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException c){
            c.printStackTrace();
        }
        return array;
    }

    //The method to write the ArrayList from the file
    public  void writeArray(ArrayList<Note> array){
        try{
            fileOutputStream = new FileOutputStream(notesfile);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(array);
            objectOutputStream.close();
            fileOutputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /*Pop up a dialog when delete button clicked
    Use the template from Google Android Developer Guide:
    http://developer.android.com/guide/topics/ui/dialogs.html
    */
    public class DialogDelete extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.delete_dialog)
                    .setPositiveButton(R.string.delete_note, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteNotes();
                        }
                    })
                    .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                        }
                    });
            // Create the AlertDialog object and return it
            return builder.create();
        }
    }

    public void addDate(View view){
        Calendar current = Calendar.getInstance();
        String date = " " + current.get(Calendar.DAY_OF_MONTH) + "/" + (current.get(Calendar.MONTH) + 1) + "/" + current.get(Calendar.YEAR);
        String text = noteTextField.getText().toString() + date;
        noteTextField.setText(text);
        noteTextField.setSelection(noteTextField.getText().length());
    }




}
